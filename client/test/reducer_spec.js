/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */
import {expect} from 'chai';
import {Map, fromJS} from 'immutable';

describe('reducer', () => {
    
    it('should handle SET_MESSAGES', () => {
        let today = new Date();
        const initialState = Map();
        const action = {type: 'SET_MESSAGES', messages: fromJS([
            {id: 1, text: 'Hello world', date: today},
            {id: 2, text: 'Heeeeyyy', date: today}
        ])};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(fromJS({
            messages: fromJS([
                {id: 1, text: 'Hello world', date: today},
                {id: 2, text: 'Heeeeyyy', date: today}
            ])
        }));
    });

    it('should handle ADD_MESSAGE', () => {
        let today = new Date();
        const initialState = fromJS({
            messages: fromJS([
                {id: 1, text: 'Hello world', date: today}
            ])
        });
        const action = {type: 'ADD_MESSAGE', message: fromJS(
            {id: 2, text: 'Heeeeyyy', date: today}
        )};
        const nextState = reducer(initialState, action);
        expect(nextState).to.equal(fromJS({
            messages: fromJS([
                {id: 1, text: 'Hello world', date: today},
                {id: 2, text: 'Heeeeyyy', date: today}
            ])
        }));
    })
});