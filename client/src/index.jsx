/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */
import React from 'react';
import DOM from 'react-dom';
import {Provider} from 'react-redux';
import store from './stores/todos';
import App from './components/App';

import TodoSocketListeners from './socket-listeners/todo';
import injectTapEventPlugin from 'react-tap-event-plugin';

TodoSocketListeners(store);
injectTapEventPlugin();

DOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    document.getElementById('app')
);