/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */
import React from 'react';
import AppBar from 'material-ui/AppBar';
import TodoList from './TodoList';
import AddTodo from './AddTodo';

import {connect} from 'react-redux';

class Main extends React.Component {
    render() {
        return (
            <div>
                <AppBar title="Chatzilla Todo" iconClassNameRight="muidocs-icon-navigation-expanded-more" />
                <TodoList todos={this.props.todos} />
                <AddTodo />
            </div>
        );
    }
}

const mapStateToProps = (todos) => todos;

export default connect(mapStateToProps)(Main);