/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */
import React from 'react';
import {Table, TableBody} from 'material-ui/Table';
import Todo from './Todo';

export default ({todos}) => {
    return (
        <Table>
            <TableBody>
                {todos.map(todo => <Todo key={todo.id} todo={todo} />)}
            </TableBody>
        </Table>
    );
}