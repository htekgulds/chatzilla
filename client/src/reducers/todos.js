/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */

export default (state = [], action) => {
    
    const todoIndex = () => state.findIndex(thisTodo => thisTodo && thisTodo.id === action.todo.id);
    
    switch (action.type) {
        case 'todo:insert': 
            return todoIndex < 0 ? [...state, action.todo] : state;
        
        case 'todo:update':
            let index = todoIndex();
            if (index > -1) {
                let updateTodo = Object.assign({}, state[index], action.todo);
                return [...state.slice(0, index), updateTodo, ...state.slice(index + 1)]
            }
            else {
                return state;
            }
            
        case 'todo:delete':
            let index = todoIndex();
            if (index > -1) {
                return [...state.slice(0, index), ...state.slice(index + 1)];
            }
            else {
                return state;
            }
            
        default: return state;
    }    
};