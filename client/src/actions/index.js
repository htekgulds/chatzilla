/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */

export const newTodo = (todo) => ({
    type: 'todo:insert',
    todo
});

export const updateTodo = (todo) => ({
    type: 'todo:update',
    todo
});

export const deleteTodo = (todo) => ({
    type: 'todo:delete',
    todo
});