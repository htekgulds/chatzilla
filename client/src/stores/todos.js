/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */

import {createStore} from 'redux';
import todos from '../reducers/todos';

export default createStore(todos);