/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */

import io from 'socket.io-client';

const socket = io(`${location.protocol}//${location.hostname}:3000`);

export default (store) => {
    socket.on('todo:insert', todo => {
        store.dispatch({ type: 'todo:insert', todo: todo });
    });

    socket.on('todo:update', todo => {
        store.dispatch({ type: 'todo:update', todo: todo });
    });

    socket.on('todo:delete', todo => {
        store.dispatch({ type: 'todo:delete', todo: todo });
    });
}