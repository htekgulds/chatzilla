/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */

import express from 'express';
import http from 'http';
import path from 'path';
import socket from 'socket.io';
import rethinkdb from 'rethinkdbdash';
import async from 'async';

import jobs from './src/async-jobs';
import changefeedSocketEvents from './src/socket-events';

let app = express();
let server = http.Server(app);
let io = socket(server);

let RDB_HOST = process.env.CI ? 'rethinkdb' : '192.168.99.100';
let r = rethinkdb({host: RDB_HOST});

async.waterfall(jobs(r), err => {
    if (err) throw err;
    console.log('Async Job finished...');

    io.on('connection', socket => {
        console.log('New client connected!');
        r.db('redx_todo').table('Todo')
            .changes({includeInitial: true, squash: true})
            .run()
            .then(changefeedSocketEvents(socket, 'todo'));
    });
});

var PORT = 3000;
server.listen(PORT);
console.log('RethinkDB Chat App running on ' + PORT);

