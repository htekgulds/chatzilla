/**
 * Created by Hasan TEKGÜL
 * on 2.06.2016.
 */
const DB = 'redx_todo';
const TABLES = ['Todo'];

export default (r) => {
    return [
        (finished) =>
            r.dbList()
                .contains(DB)
                .run()
                .then(result => finished(null, result))
                .error(err => finished(err)),

        (dbExists, finished) => {
            if (dbExists == false) {
                r.dbCreate(DB)
                    .run()
                    .then(() => finished(null))
                    .error(err => finished(err));
            }
            else {
                finished(null)
            }
        },

        (finished) =>
            r.db(DB)
                .tableList()
                .contains(TABLES[0])
                .run()
                .then(result => finished(null, result))
                .error(err => finished(err)),

        (tableExists, finished) => {
            if (tableExists == false) {
                r.db(DB)
                    .tableCreate(TABLES[0])
                    .run()
                    .then(result => finished(null, false))
                    .error(err => finished(err));
            }
            else {
                finished(null, true);
            }
        },
        
        (tableIndexed, finished) => {
            if (tableIndexed == false) {
                r.db(DB)
                    .table(TABLES[0])
                    .indexCreate('date')
                    .run()
                    .then(result => finished(null))
                    .error(err => finished(err));
            }
            else {
                finished(null);
            }
        }
    ]
};